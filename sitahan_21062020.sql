-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 21 Jun 2020 pada 13.11
-- Versi server: 5.7.24
-- Versi PHP: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sitahan_v2`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `tahanan_id` int(11) DEFAULT NULL,
  `start_date_old` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `end_date_old` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `start_date_new` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `end_date_new` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `logs`
--

INSERT INTO `logs` (`id`, `user_id`, `tahanan_id`, `start_date_old`, `end_date_old`, `start_date_new`, `end_date_new`, `created`, `updated`) VALUES
(1, 12, 4, '2019-12-30 00:00:00', '2019-12-30 00:00:00', '2019-12-29 00:00:00', '2019-12-30 00:00:00', '2020-01-26 05:19:06', NULL),
(2, 12, 4, '2019-12-29 00:00:00', '2019-12-30 00:00:00', '2020-02-01 00:00:00', '2020-02-29 00:00:00', '2020-01-26 05:19:47', NULL),
(3, 12, 4, '2020-01-26 00:00:00', '2020-01-26 00:00:00', '2020-04-02 00:00:00', '2020-07-31 00:00:00', '2020-01-26 07:11:32', NULL),
(4, 12, 4, '2020-01-26 00:00:00', '2020-01-26 00:00:00', '2020-01-01 00:00:00', '2020-12-31 00:00:00', '2020-01-26 07:12:32', NULL),
(5, 12, 6, '2020-02-07 00:00:00', '1970-01-01 00:00:00', '2020-02-07 00:00:00', '2020-02-28 00:00:00', '2020-04-02 05:05:21', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_file_berkas`
--

CREATE TABLE `tbl_file_berkas` (
  `id` int(11) NOT NULL,
  `id_surat` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `user_created` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_file_berkas`
--

INSERT INTO `tbl_file_berkas` (`id`, `id_surat`, `file`, `created`, `user_created`) VALUES
(6, '7', '9597df05e19388b2c0a10f34a67225e0.pdf', '2019-12-31 08:46:20', '12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_ref_instansi`
--

CREATE TABLE `tbl_ref_instansi` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `level` enum('0','1','2') DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_ref_instansi`
--

INSERT INTO `tbl_ref_instansi` (`id`, `kode`, `nama`, `alamat`, `level`, `created`, `updated`) VALUES
(2, '000', 'Super Admin', 'Super Admin', '0', '2019-12-09 01:08:28', '2019-12-29 09:52:25'),
(4, '007', 'Lembaga Pemasyarakatan Perempuan Kelas III Manokwari', 'solo', '2', '2019-12-09 01:08:28', '2020-01-01 08:46:36'),
(7, '008', ' PENGADILAN NEGERI MANOKWARI ', '-', '1', '2019-12-20 14:16:26', '2020-06-21 12:41:13');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_ref_penerima`
--

CREATE TABLE `tbl_ref_penerima` (
  `id` int(11) NOT NULL,
  `nama_penerima` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `instansi` varchar(255) DEFAULT NULL,
  `jabatan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_ref_penerima`
--

INSERT INTO `tbl_ref_penerima` (`id`, `nama_penerima`, `created`, `updated`, `instansi`, `jabatan`) VALUES
(5, 'Drs. La Ode Mustari, M.Si.', '2019-11-07 00:36:43', NULL, NULL, NULL),
(6, 'H. M. Faisal Laimu, S.E., M.Si', '2019-11-07 00:36:57', NULL, NULL, NULL),
(7, 'Dr. Ir. H. Ila Ladamay, M.Si.', '2019-11-07 00:37:07', NULL, NULL, NULL),
(8, 'Agus Feisal Hidayat, S.Sos., M.Si.', '2019-11-07 00:37:19', NULL, NULL, NULL),
(9, 'H. La Ode Arusani', '2019-11-07 00:37:30', NULL, NULL, NULL),
(10, 'Syaukani Saleh', '2019-11-07 00:37:47', NULL, NULL, NULL),
(11, 'Syaiful Emran Ali', '2019-11-07 00:37:59', NULL, NULL, NULL),
(12, 'Warman Suwardi', '2019-11-07 00:38:13', NULL, NULL, NULL),
(13, 'Barlian Pintarudin', '2019-11-07 00:38:24', NULL, NULL, NULL),
(14, 'Hermen Malik', '2019-11-07 00:38:35', NULL, NULL, NULL),
(15, 'Gusril Pausi', '2019-11-07 00:38:48', NULL, NULL, NULL),
(16, NULL, '2019-12-08 12:57:16', NULL, NULL, NULL),
(17, 'asa', '2019-12-08 13:03:06', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_ref_surat`
--

CREATE TABLE `tbl_ref_surat` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_ref_surat`
--

INSERT INTO `tbl_ref_surat` (`id`, `kode`, `nama`, `created`, `updated`) VALUES
(1, 'HK.01', 'dinas pengadilan nasional', '2019-11-18 13:00:14', '2019-11-18 00:00:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_surat`
--

CREATE TABLE `tbl_surat` (
  `id_surat` int(10) NOT NULL,
  `no_surat` varchar(50) DEFAULT NULL,
  `asal_surat` int(11) NOT NULL,
  `isi` mediumtext NOT NULL,
  `perihal` varchar(30) NOT NULL,
  `indeks` varchar(30) NOT NULL,
  `tgl_surat` date NOT NULL,
  `tgl_diterima` date NOT NULL,
  `file` varchar(250) NOT NULL,
  `hal` varchar(250) NOT NULL,
  `pengirim` int(11) NOT NULL,
  `alamat_pengirim` varchar(255) DEFAULT NULL,
  `tembusan` varchar(255) DEFAULT NULL,
  `kepada` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `upload_berkas` varchar(255) DEFAULT NULL,
  `status_kirim` varchar(255) DEFAULT NULL,
  `id_created` int(11) NOT NULL,
  `instansi` int(11) NOT NULL,
  `status_delete` int(5) NOT NULL,
  `status_view` int(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_surat`
--

INSERT INTO `tbl_surat` (`id_surat`, `no_surat`, `asal_surat`, `isi`, `perihal`, `indeks`, `tgl_surat`, `tgl_diterima`, `file`, `hal`, `pengirim`, `alamat_pengirim`, `tembusan`, `kepada`, `created`, `updated`, `upload_berkas`, `status_kirim`, `id_created`, `instansi`, `status_delete`, `status_view`) VALUES
(2, '92309213/12921.m/3209', 4, '', 'pengadaan baru', '', '2020-01-01', '2020-01-01', '482f10e20b7e6c5cd2d432e806575652.docx', 'penambahan masa tahananaaa', 12, NULL, 'dkk', '1', '2020-06-21 18:58:25', '2020-06-21 09:58:25', '1', '1', 12, 2, 0, 1),
(3, '240934/2923.kkda/219', 2, '', 'pembaharuan', '', '2020-01-01', '0000-00-00', '65f1fc5574d9480a6cccccaa82cd525f.docx', '----', 12, 'solo', 'saya', '1', '2020-06-21 12:09:41', '2020-06-21 03:09:41', '1', '1', 12, 2, 0, 1),
(4, 'a213', 1, '', 'asas', '', '2020-01-02', '0000-00-00', 'b156d086b70a43716598e1b407d1a7a2.docx', 'sa', 18, NULL, 'as', '2', '2020-01-02 23:34:55', '2020-01-02 14:34:55', '1', NULL, 18, 1, 0, 0),
(5, '21323', 1, '', '3123', '', '2020-01-02', '2020-01-02', '62bb4fc226e7ed50217e2248f217a258.docx', '231', 18, '-', '23213', '2', '2020-06-21 12:11:42', '2020-06-21 03:11:42', '1', '1', 18, 1, 0, 1),
(6, '546546', 1, '', 'sas', '', '2020-01-02', '0000-00-00', 'b59b83872fe06dc0ebb3304586225e86.png', 'as', 18, '-', 'asas', '2', '2020-01-03 01:21:40', '2020-01-02 16:21:40', '1', NULL, 18, 1, 0, 0),
(7, '01.1.2020/PH/TU', 2, '', 'Tahan', '', '2020-06-21', '2020-06-21', 'd9aab5dccd0528e5b76f7ce9efda6b4d.pdf', 'Ada deh', 13, 'solo', 'Kepala', '1', '2020-06-21 21:09:37', '2020-06-21 12:09:37', '1', '1', 13, 2, 0, 1),
(8, '01.1.2020/LPS/TU', 1, '', 'Permohonan Ke Pengadilan', '', '2020-06-21', '2020-06-21', '0f82764c6ee8111b52d341d43590d2a0.pdf', 'Permohonan Diterima', 19, '-', 'Kepala', '2', '2020-06-21 21:12:49', '2020-06-21 12:12:49', '1', '1', 19, 1, 0, 1),
(9, '02.2.2020/PH/TU', 1, '', 'Perpanjangan ke Lapas', '', '2020-06-21', '2020-06-21', '992aa3db3e43232200cffb3509c84325.pdf', 'Silahkan Perpanjang Tahanan Eduardus', 18, '-', 'Kepala Pengadilan', '2', '2020-06-21 21:35:20', '2020-06-21 12:35:20', '1', '1', 18, 1, 0, 1),
(10, '02.2.2020/LPS/TU', 2, '', 'Balasan Perpanjangan Oleh Lapa', '', '2020-06-21', '2020-06-21', '9199f8e01550411ad6b2f6f286525a0d.pdf', 'Surat anda sudah kami terima', 12, 'solo', 'Ketua Pengadilan', '1', '2020-06-21 21:39:12', '2020-06-21 12:39:12', '1', '1', 12, 2, 0, 1),
(11, '03.2.2020/LPS/TU', 1, '', 'Perpanjangan ke Lapas', '', '2020-06-21', '2020-06-21', 'b3eedb918046a0e34bf5b6b77e53e2ff.pdf', 'Tolong di panjangkan', 18, '-', 'Kepala Lapas', '2', '2020-06-21 22:06:39', '2020-06-21 13:06:39', '1', '1', 18, 1, 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_surat_keluar`
--

CREATE TABLE `tbl_surat_keluar` (
  `id_surat` int(10) NOT NULL,
  `no_surat` varchar(50) DEFAULT NULL,
  `asal_surat` varchar(250) NOT NULL,
  `isi` mediumtext NOT NULL,
  `kode_hal` varchar(30) NOT NULL,
  `indeks` varchar(30) NOT NULL,
  `tgl_surat` date NOT NULL,
  `tgl_diterima` date NOT NULL,
  `file` varchar(250) NOT NULL,
  `file2` varchar(255) DEFAULT NULL,
  `file3` varchar(255) DEFAULT NULL,
  `hal` varchar(250) NOT NULL,
  `pengirim` varchar(30) NOT NULL,
  `alamat_pengirim` varchar(255) DEFAULT NULL,
  `tembusan` varchar(255) DEFAULT NULL,
  `kepada` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `upload_berkas` varchar(255) DEFAULT NULL,
  `status_kirim` varchar(255) DEFAULT NULL,
  `id_created` int(11) NOT NULL,
  `instansi` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_surat_keluar`
--

INSERT INTO `tbl_surat_keluar` (`id_surat`, `no_surat`, `asal_surat`, `isi`, `kode_hal`, `indeks`, `tgl_surat`, `tgl_diterima`, `file`, `file2`, `file3`, `hal`, `pengirim`, `alamat_pengirim`, `tembusan`, `kepada`, `created`, `updated`, `upload_berkas`, `status_kirim`, `id_created`, `instansi`) VALUES
(1, '/1/HK.01/12/19', 'adminlppm', '', '1', '', '2019-12-11', '0000-00-00', 'b410a62e36c6cc141dcb103f18247979.jpg', '', '', 'a', 'adminlppm', '-', 'a', 'H. La Ode Arusani', '2019-12-13 09:13:08', '2019-12-12 22:13:08', '1', '1', 12, NULL),
(2, '398123.3091203/213921', 'Operator Pengadilan', '', '-', '', '2019-12-21', '0000-00-00', 'a75579f1df3f71e40adcdacead512ed1.jpg', '', '', '--', 'Operator Pengadilan', NULL, '--', '3', '2019-12-21 02:19:45', NULL, NULL, NULL, 18, NULL),
(3, '398123.3091203/213921', 'Operator Pengadilan', '', '-', '', '2019-12-21', '0000-00-00', 'd718d423b2878ea8c1fd718be5932b77.docx', '', '', '--', 'Operator Pengadilan', NULL, '--', '3', '2020-01-01 18:09:24', '2020-01-01 09:09:24', '1', '1', 18, NULL),
(4, '398123.3091203/213921', 'Operator Pengadilan', '', '-', '', '2019-12-21', '0000-00-00', '8bc525283286f556cc40029acb1d9667.jpg', '', '', '--', 'Operator Pengadilan', NULL, '--', '3', '2019-12-21 02:20:38', NULL, NULL, NULL, 18, NULL),
(5, '2103231/3921.213219/', '18', '', '--', '', '2019-12-21', '0000-00-00', 'c97b75688b818f021b3494d503a6889a.jpg', '', '', '--', 'Operator Pengadilan', NULL, '-', '5', '2019-12-21 02:23:08', NULL, NULL, NULL, 18, NULL),
(6, 'aaa', '18', '', 'a', '', '2019-12-30', '0000-00-00', '', '', '', 'a', 'admin pengadilan', NULL, 'a', '2', '2019-12-30 09:11:25', NULL, NULL, NULL, 18, NULL),
(7, 'a', '12', '', '1', '', '2019-12-30', '0000-00-00', '', '', '', '1', 'adminlppm', '-', '1', NULL, '2019-12-31 15:07:05', '2019-12-31 06:07:05', '1', '1', 12, '2'),
(8, '653', '18', '', '54', '', '2019-12-30', '0000-00-00', '', '', '', '123', 'admin pengadilan', NULL, '213', '4', '2019-12-30 10:26:28', NULL, NULL, NULL, 18, '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_surat_masuk`
--

CREATE TABLE `tbl_surat_masuk` (
  `id_surat` int(10) NOT NULL,
  `no_surat` varchar(50) DEFAULT NULL,
  `asal_surat` varchar(250) NOT NULL,
  `isi` mediumtext NOT NULL,
  `kode_hal` varchar(30) NOT NULL,
  `indeks` varchar(30) NOT NULL,
  `tgl_surat` date NOT NULL,
  `tgl_diterima` date NOT NULL,
  `file` varchar(250) NOT NULL,
  `file2` varchar(255) DEFAULT NULL,
  `file3` varchar(255) DEFAULT NULL,
  `hal` varchar(250) NOT NULL,
  `pengirim` varchar(30) NOT NULL,
  `alamat_pengirim` varchar(255) DEFAULT NULL,
  `tembusan` varchar(255) DEFAULT NULL,
  `kepada` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `berkas_disposisi` varchar(255) DEFAULT NULL,
  `status_disposisi` enum('0','1') DEFAULT NULL,
  `id_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_surat_masuk`
--

INSERT INTO `tbl_surat_masuk` (`id_surat`, `no_surat`, `asal_surat`, `isi`, `kode_hal`, `indeks`, `tgl_surat`, `tgl_diterima`, `file`, `file2`, `file3`, `hal`, `pengirim`, `alamat_pengirim`, `tembusan`, `kepada`, `created`, `updated`, `berkas_disposisi`, `status_disposisi`, `id_created`) VALUES
(1, 'a', 'a', '', 'a', '', '2019-12-11', '0000-00-00', '6e857311c5813e051eb0140673d51687.jpg', NULL, NULL, 'aaaa', 'a', 'a', NULL, NULL, '2019-12-11 00:03:20', NULL, NULL, NULL, 12),
(2, 'a', 'a', '', 'a', '', '2019-12-11', '0000-00-00', '468d569ceac7511c6a156c93ac0a3dd1.jpg', NULL, NULL, 'aaaa', 'a', 'a', NULL, NULL, '2019-12-11 00:04:24', NULL, NULL, NULL, 12);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_tahanan`
--

CREATE TABLE `tbl_tahanan` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tgl_lahir` varchar(255) DEFAULT NULL,
  `jenis_kelamin` varchar(255) DEFAULT NULL,
  `kebangsaan` varchar(255) DEFAULT NULL,
  `tempat_tinggal` varchar(255) DEFAULT NULL,
  `agama` varchar(255) DEFAULT NULL,
  `pekerjaan` varchar(255) DEFAULT NULL,
  `id_instansi` int(11) DEFAULT NULL,
  `pejabat` varchar(255) DEFAULT NULL,
  `perkara` varchar(255) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `end_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status_change` int(5) DEFAULT NULL,
  `tgl_surat` datetime DEFAULT NULL,
  `no_surat` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_tahanan`
--

INSERT INTO `tbl_tahanan` (`id`, `nama`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `kebangsaan`, `tempat_tinggal`, `agama`, `pekerjaan`, `id_instansi`, `pejabat`, `perkara`, `start_date`, `end_date`, `status_change`, `tgl_surat`, `no_surat`, `created`, `updated`) VALUES
(4, 'agussss', 'solo', '2019-12-21', 'perempuan', 'indonesia', 'solosololosolosololosolosololosolosololosolosololosolosololosolosololosolosololo', 'islam', 'wiraswasta', 4, 'hanum, S, Sos', 'pasal 31 ', '2020-04-02 16:16:15', '2020-04-02 16:16:15', 1, NULL, NULL, '2020-04-02 16:16:15', '2020-04-02 07:16:15'),
(5, 'daisy', 'Sorong', '2019-12-30', 'perempuan', 'Indonesia', 'sorong', 'islam', 'Tidak Ada', 4, 'Pengadilan Manokwari', 'Tipikor', '2019-12-29 00:00:00', '2020-05-25 00:00:00', NULL, NULL, NULL, '2019-12-29 12:35:26', NULL),
(6, '11', '12', '2020-01-27', 'perempuan', '12', '12', '12', '12', 4, '12', '12', '2020-02-07 00:00:00', '2020-02-28 00:00:00', 1, '2019-02-01 00:00:00', '122222222222222', '2020-04-02 10:05:21', '2020-04-02 01:05:21');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(64) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `role` enum('1','2','3') NOT NULL COMMENT '1. Super Admin; 2. Admin, 3. Operator',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('1','0') DEFAULT '1' COMMENT '1. on; 0. off',
  `jabatan` varchar(255) DEFAULT NULL,
  `nip` varchar(18) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `instansi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id_user`, `username`, `password`, `nama`, `role`, `created`, `updated`, `status`, `jabatan`, `nip`, `alamat`, `instansi`) VALUES
(11, 'superadmin', '17c4520f6cfd1ab53d8745e84681eb49', 'superadmin', '1', '2019-12-09 01:09:28', NULL, '1', '-', '-', 'Manokwari', 2),
(12, 'adminlppm', '2e3e6e77a33c22f493039d68998be0ee', 'adminlppm', '2', '2019-12-29 22:40:39', '2019-12-30 04:40:39', '1', 'adminlppm', '-', '-', 4),
(13, 'operatorlppm', '25d13026e2443728f69cb1fd12008829', 'operatorlppm', '3', '2019-12-09 01:16:49', NULL, '1', '-', '-', '-', 4),
(18, 'adminpengadilan', '142a52092538a3842cb732dfe510e8ba', 'admin pengadilan', '2', '2019-12-20 14:17:37', '2019-12-29 10:08:19', '1', 'IT', '20391839123', NULL, 7),
(19, 'operatorpengadilan', 'd34c2380ce945b168a634a9c9b82e467', 'Operator Pengadilan', '3', '2019-12-29 01:10:15', '2019-12-30 02:48:20', '1', 'Operator Situs', '199701222019021001', NULL, 7),
(20, 'a', '92eb5ffee6ae2fec3ad71c777531578f', 'k', '3', '2020-06-20 07:49:22', '2020-06-20 12:49:23', '1', 'k', '1', NULL, 7),
(21, 'p', '83878c91171338902e0fe0fb97a8c47a', 'p', '2', '2020-06-20 07:50:29', NULL, '1', 'p', '1', NULL, 2),
(22, 'hi', '49f68a5c8493ec2c0bf489821c21fc3b', 'hi', '3', '2020-06-20 08:02:16', NULL, '1', 'hi', '123', NULL, 7),
(23, 'g', 'b2f5ff47436671b6e533d8dc3614845d', 'g', '3', '2020-06-20 08:04:20', NULL, '1', 'g', '1', NULL, 4),
(24, 'operatorpengadilan2', '202cb962ac59075b964b07152d234b70', 'Alex', '3', '2020-06-21 03:17:17', NULL, '1', 'Panitra 1', '1989092019021002', NULL, 7),
(25, 'agung.septyanto', '202cb962ac59075b964b07152d234b70', 'Agung Septyanto', '3', '2020-06-21 03:19:04', NULL, '1', 'Pengelola', '199509272019021001', NULL, 7),
(26, 'operator3', '202cb962ac59075b964b07152d234b70', 'Agung Septyanto', '3', '2020-06-21 03:55:17', '2020-06-21 12:55:17', '1', 'Operator Situs', '199509272019021001', NULL, 4);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_file_berkas`
--
ALTER TABLE `tbl_file_berkas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_ref_instansi`
--
ALTER TABLE `tbl_ref_instansi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_ref_penerima`
--
ALTER TABLE `tbl_ref_penerima`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_ref_surat`
--
ALTER TABLE `tbl_ref_surat`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_surat`
--
ALTER TABLE `tbl_surat`
  ADD PRIMARY KEY (`id_surat`),
  ADD KEY `id_created` (`id_created`);

--
-- Indeks untuk tabel `tbl_surat_keluar`
--
ALTER TABLE `tbl_surat_keluar`
  ADD PRIMARY KEY (`id_surat`),
  ADD KEY `id_created` (`id_created`);

--
-- Indeks untuk tabel `tbl_surat_masuk`
--
ALTER TABLE `tbl_surat_masuk`
  ADD PRIMARY KEY (`id_surat`),
  ADD KEY `id_created` (`id_created`);

--
-- Indeks untuk tabel `tbl_tahanan`
--
ALTER TABLE `tbl_tahanan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`) USING BTREE,
  ADD KEY `instansi` (`instansi`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tbl_file_berkas`
--
ALTER TABLE `tbl_file_berkas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tbl_ref_instansi`
--
ALTER TABLE `tbl_ref_instansi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tbl_ref_penerima`
--
ALTER TABLE `tbl_ref_penerima`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `tbl_ref_surat`
--
ALTER TABLE `tbl_ref_surat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_surat`
--
ALTER TABLE `tbl_surat`
  MODIFY `id_surat` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `tbl_surat_keluar`
--
ALTER TABLE `tbl_surat_keluar`
  MODIFY `id_surat` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tbl_surat_masuk`
--
ALTER TABLE `tbl_surat_masuk`
  MODIFY `id_surat` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_tahanan`
--
ALTER TABLE `tbl_tahanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tbl_surat`
--
ALTER TABLE `tbl_surat`
  ADD CONSTRAINT `tbl_surat_ibfk_1` FOREIGN KEY (`id_created`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tbl_surat_keluar`
--
ALTER TABLE `tbl_surat_keluar`
  ADD CONSTRAINT `tbl_surat_keluar_ibfk_1` FOREIGN KEY (`id_created`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tbl_surat_masuk`
--
ALTER TABLE `tbl_surat_masuk`
  ADD CONSTRAINT `tbl_surat_masuk_ibfk_1` FOREIGN KEY (`id_created`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`instansi`) REFERENCES `tbl_ref_instansi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
