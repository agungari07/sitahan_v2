<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
	    parent:: __construct();
	    $this->load->library(array('form_validation'));
	    if (!$this->session->userdata('logged_in'))
	    {
	        redirect(base_url('login'));
	    }

	    $this->id_user = $this->session->userdata('id_user');
	    $this->role = $this->session->userdata('role');
	    $this->load->model('Surat_keluar_model', 'surat_keluar', true);
	}


	public function index()
	{
		
		$data = array(
			'judul' => "Dashboard",
		);
		

		$this->load->view('dashboard/view', $data);
	}

	public function message_inbox(){
		$data = $this->surat_keluar->masuk_list_inbox($this->session->userdata('id_user'), $this->session->userdata('level_instansi'));
		// var_dump(count($data));exit();
		if (count($data) > 0) {
			foreach ($data as $key) {
				$output[] = '
				<li>
	                <a href='.base_url("surat_masuk/detail/").$key->id_surat.'>
	                    <p>'.$key->perihal.'<br>
	                    '.$key->nama_instansinya.'</p>
	                </a>
	            </li>
				';
			}
		}else{
			$output = '<li><a href="#" class="text-bold text-italic">No Notification Found</a></li>';
		}

		$count = count($data);
		$data = array(
			'notification'   		=> $output,
			'unseen_notification' 	=> $count
		);
		echo json_encode($data);
	}



}

