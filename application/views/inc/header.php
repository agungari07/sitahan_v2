<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url();?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>SITAHAN</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>SITAHAN</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="<?php echo base_url(); ?>assets/#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown">
                   <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="label label-pill label-danger count" style="border-radius:10px;"></span> <span class="glyphicon glyphicon-envelope" style="font-size:18px;"></span></a>
                   <ul class="dropdown-menu"></ul>
                </li>
                
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <b>Selamat Datang,  <span class="hidden-xs"><?= $this->session->userdata('username') ?></span></b>
                </a>
                </li>
                <li>
                    <a href="<?= base_url('logout')?>">
                <i class="fa fa-sign-out"></i> <span>Keluar</span>
              </a>
                </li>
            </ul>
        </div>
    </nav>
</header>
